<?php

// EDSU_AUTH_NAME defined in qa-external-users.php

class edsu_auth {
	public function allow_template($template) {
        return true;
    }

	public function allow_region($region) {
		return ($region === 'side');
	}

	public function output_widget($region, $place, $themeobject, $template, $request, $qa_content) {
		require_once QA_INCLUDE_DIR.'app/users.php';

        $themeobject->output('<div class="edsu-auth">');
        
        if (qa_get_logged_in_userid() == null) {
            edsu_auth_show_auth($themeobject);
        } else {
            edsu_auth_show_logged_in($themeobject);
        }

        $themeobject->output('</div>');
	}
}


function edsu_auth_show_auth($themeobject) {
?>
    <script src="https://edsu-cdn.org/libs/edsu-raw/0.1/edsu-raw.min.js"></script>
    <script src="https://edsu-cdn.org/libs/edsu-grant/0.1/edsu-grant.min.js"></script>
    <div class="widget-spacer">
    <div class="widget-feedback" style="display: none"></div>
    <div class="blurb">
        <p>This is an <a href="https://edsu.org/what-is-an-edsu-app/">Edsu App</a>.  Which means
        that to participate you don't need to register, just put in your Edsu username above.</p>

        <p>If you'd like to get an Edsu account, click <a href="https://edsu.org/find/">here</a>
        to find a provider.</p>
    </div>
    <div class="widget">
        <a href="https://edsu.org/what-is-an-edsu-app/">
            <svg style="vertical-align: bottom"
                width="34" height="38" viewBox="0 0 34 38" xmlns="http://www.w3.org/2000/svg">
                <path fill="#444" d="m0 0v38h34v-38zm6 2h10v4h-10v2h6v4h-6v2h10v4h-10a4 4 0 0 1-4-4v-8a4
                4 0 0 1 4-4zm12 0h10a4 4 0 0 1 4 4v8a4 4 0 0 1-4 4h-10v-12zm4 4v8h6v-8zm-16
                14h10v4h-10v2h6a4 4 0 0 1 4 4v2a4 4 0 0 1-4 4h-10v-4h10v-2h-6a4 4 0 0 1-4-4v-2a4 4 0 0 1
                4-4zm12 0h4v12h6v-12h4v12a4 4 0 0 1-4 4h-6a4 4 0 0 1-4-4z"></svg>
        </a>
        <input type="email" placeholder="you@example.com">
        <button>&rarr;</button>
    </div>
    <script>

document.addEventListener('DOMContentLoaded', () => {
    // Nix the cookie, in case it's a failed auth or log-out, as opposed to a non-auth
    document.cookie = 'edsu_auth_v1=;expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/';

    let widgetEl = document.querySelector('.edsu-auth .widget');
    let inputEl = widgetEl.querySelector('input');
    let buttonEl = widgetEl.querySelector('button');
    let feedbackEl = document.querySelector('.edsu-auth .widget-feedback');
    inputEl.addEventListener('focus', () => widgetEl.classList.add('shown'));
    inputEl.addEventListener('blur', () => {
        setTimeout(() => {
            if ([buttonEl, inputEl].indexOf(document.activeElement) == -1)
                widgetEl.classList.remove('shown');
        }, 100);
    });

    // Fn to display the feedback
    let showFeedback = e => {
        let html;
        if (e.err)
            html = 'There was an error: ' + e.err.toString() + ', ' + e.debug;
        else
            html = String(e);
        feedbackEl.style.display = 'block';
        feedbackEl.innerHTML = html;
    };

    // Fn to do the Edsu grant request
    let getGrant = () => {
        feedbackEl.style.display = 'none';
        let username = String(inputEl.value).trim();
        if (!Edsu.validateUsername(username)) {
            showFeedback('Please enter a valid Edsu username');
            inputEl.focus();
        } else {
            buttonEl.focus();

            // Send the grant request to the auth tab
            let label = 'Edsu Q&A';
            let explanation = "This app only uses Edsu for authentication, so we just " +
                "need a private name to prove that you're you.";
            let name = '<?php echo(EDSU_AUTH_NAME) ?>';
            let grantReq =  {
                capabilitiesOwner: {
                    'edsu:version': '0.1',
                    'edsu:name-get': [name],
                    'edsu:name-put': [name + ' destructive'],
                },
                label,
                explanation,
            }
            edsuAsyncGrantRequest(username, grantReq).then(resp => {
                // Got the token, save it to the cookie
                let cookie = `edsu_auth_v1=${username}:${resp.tokenOwner}` +
                             ';samesite=strict;path=/;domain=' + location.host;
                if (resp.expires)
                    cookie += ';max-age=' + resp.expires;
                document.cookie = cookie;
                
                // Since we're in the sidebar, reload to re-render the page now we're logged in
                location.reload();
            }).catch(showFeedback);
        }
    };

    // Kick off getGrant from events
    buttonEl.addEventListener('click', getGrant);
    inputEl.addEventListener('keypress', e => {
        if (e.keyCode == 13 || e.which == 13)
            getGrant();
        else
            feedbackEl.style.display = 'none';
    });
});

    </script>
<?php
}


function edsu_auth_show_logged_in($themeobject) {
?>
<script>
EdsuLog = console.log;
async function edsuLogOut() {
    // Amend the cookie to include the log-out "command", then reload          
    let re = /edsu_auth_v1=([^;]+)(;|$)/.exec(String(document.cookie));
    let val = re[1] ? re[1] + ':log-out' : '';
    document.cookie = 'edsu_auth_v1=' + val + ';max-age=16;path=/;domain=' + location.host;
    location.reload();
}
</script>
<?php
    $themeobject->output('<h2 class="your-account-heading">Your Account</h2>');
    $link = '/user/' . urlencode(qa_get_logged_in_userid());
    $themeobject->output("<p><a href='$link'>" . qa_get_logged_in_userid() . '</a></p>');
    $themeobject->output('<p><a href="javascript:edsuLogOut()">Log Out</a></p>');
}
