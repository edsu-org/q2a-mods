<?php


if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
    header('Location: ../');
    exit;
}


define('EDSU_AUTH_CONN_TIMEOUT_S', 16);
define('EDSU_AUTH_DURATION_S', 4096);
define('EDSU_AUTH_META_KEY', 'auth_v1');
define('EDSU_AUTH_COOKIE_NAME', 'edsu_auth_v1');
define('EDSU_AUTH_NAME', 'prv.app.edsu-org.QnA.v1.authenticated');
define('EDSU_AUTH_NULL_ESON_HASH', 'QmQXLpE7va7nHg2mYqtoafzxutwiEMmLp83SWpryZQmxND');
define('EDSU_AUTH_TOKEN_DEL_ROOT', 'pub.srv.edsu.authentication.tokens.delete.');



function qa_get_logged_in_user() {
    $levels = array(
        'heath@edclave.com' => QA_USER_LEVEL_ADMIN,
    );

    if (isset($_COOKIE[EDSU_AUTH_COOKIE_NAME])) {
        // Parse cookie
        $split = explode(":", $_COOKIE[EDSU_AUTH_COOKIE_NAME]);
        if (count($split) < 2)
            return null;

        // - Token
        $token = $split[1];
        if (!preg_match('/^[a-zA-Z0-9_-]+$/', $token))
            return false;

        // - Username
        $username = $split[0];
        if (!preg_match('/^([a-z0-9-]+)@([a-z0-9-]+[.][a-z0-9-]+)$/', $username, $username_parts))
            return false;

        // Either perform auth, or log out, depending on the third part (or absence thereof)
        require_once QA_INCLUDE_DIR . 'db/metas.php';
        if (count($split) == 2) {
            // Logged in
            if (!edsu_authenticate_from_token($username, $username_parts, $token))
                return null;

            // Assign caps level
            if (array_key_exists($username, $levels)) {
                $level = $levels[$username];
            } else {
                $level = QA_USER_LEVEL_BASIC;
            }
            return array(
                'userid' => $username,
                'publicusername' => $username,
                'email' => null,
                'level' => $level,
            );
        } else if (count($split) == 3 && $split[2] == 'log-out') {
            // Requesting a log out
            edsu_log_out($username, $username_parts, $token);
            return null;
        } else {
            return null;
        }
    } else {
        return null;
    }
}


function edsu_authenticate_from_token($username, $username_parts, $token) {
    // See if the meta's cache is enough to authenticate for now
    $meta = qa_db_usermeta_get($username, EDSU_AUTH_META_KEY);
    if ($meta) {
        [$stored_token, $created] = explode(':', $meta);
        $expires = intval($created) + EDSU_AUTH_DURATION_S;
        if ($token == $stored_token && time() < $expires) {
            // Token matches, and the auth hasn't expired yet - all good
            return true;
        }
    }
    
    // Otherwise, hit up their Edsu account to see if they (still) approve
    if (edsu_confirm_name_caps($username, $username_parts, $token))  {
        // OK, good, cache the results for a while
        $created = time();
        qa_db_usermeta_set($username, EDSU_AUTH_META_KEY, "$token:$created");
        return true;
    } else {
        // Nope, no auth
        return false;
    }
}


// There's no PHP Edsu client library yet, so this is just talking straight to the server
function edsu_confirm_name_caps($username, $username_parts, $token) {

    $sock = edsu_connect($username, $username_parts, $token);
    if (!$sock)
        return false;

    try {
        fwrite($sock, "edsu hello\nversions 0.1\ntoken $token\n\n");

        // Wait for auth
        while (($line = fgets($sock)) !== false) {
            if ($line == "edsu authenticated\n") {
                break;
            } else if ($line == "edsu oob\n") {
                return false;
            }
        }

        // Get name
        $name = EDSU_AUTH_NAME;
        fwrite($sock, "edsu name-get\nname $name\n\n");
        while (($line = fgets($sock)) !== false) {
            // We got the name - we're authenticated :)
            if ($line == "edsu name\n") {
                return true;
            // If it's an error, then we need to keep going
            } else if ($line == "edsu oob\n") {
                break;
            }
        }

        // See if the OOB was a not-found
        while (($line = fgets($sock)) !== false) {
            if (substr($line, 0, 5) == 'code ') {
                if ($line == "code not-found\n") {
                    break;
                } else {
                    // Anything other than a not-found, and we're calling it not-authed
                    return false;
                }
            }
        }

        // Try a name-put
        // - Put a null ESON block
        fwrite($sock, "edsu block-put\npayload-stop 3\n\n~\n\n");
        while (($line = fgets($sock)) !== false) {
            if ($line == "edsu ok\n") {
                break;
            } else if ($line == "edsu oob\n") {
                return false;
            }
        }

        // - Then name it
        fwrite($sock, "edsu name-put\nname $name\n" .
                      "hash " . EDSU_AUTH_NULL_ESON_HASH . "\n\n");

        while (($line = fgets($sock)) !== false) {
            // We put the name - we're authenticated :)
            if ($line == "edsu ok\n") {
                return true;
            // If it's an error, then we need to keep going
            } else if ($line == "edsu oob\n") {
                return false;
            }
        }
    } finally {
        fclose($sock);
    }
}


function edsu_log_out($username, $username_parts, $token) {
    // Nix the meta
    $meta = qa_db_usermeta_get($username, EDSU_AUTH_META_KEY);
    if (!$meta)
        return;

    // Authenticate
    [$stored_token, $created] = explode(':', $meta);
    if ($token != $stored_token)
        return;

    // Clear the meta
    qa_db_usermeta_clear($username, EDSU_AUTH_META_KEY);

    // Attempt to remove the token and name that we created
    edsu_remove_name_and_token($username, $username_parts, $token);
}


function edsu_remove_name_and_token($username, $username_parts, $token) {

    $sock = edsu_connect($username, $username_parts, $token);
    if (!$sock)
        return;

    // Connect to Edsu daemon
    try {
        fwrite($sock, "edsu hello\nversions 0.1\ntoken $token\n\n");

        // Wait for auth
        while (($line = fgets($sock)) !== false) {
            if ($line == "edsu authenticated\n") {
                break;
            } else if ($line == "edsu oob\n") {
                // If it doesn't auth, then there's nothing we can do anyway
                return;
            }
        }

        // Delete name
        $name = EDSU_AUTH_NAME;
        fwrite($sock, "edsu name-put\nname $name\n" .
                      "existing-hash " . EDSU_AUTH_NULL_ESON_HASH . "\n\n");
        while (($line = fgets($sock)) !== false) {
            if ($line == "edsu ok\n" || $line == "edsu oob\n") {
                // This is best efforts, so regardless of outcome, keep going   
                break;
            }
        }

        // Delete token
        $tok_del_name = EDSU_AUTH_TOKEN_DEL_ROOT . 'owner.' . $token;
        fwrite($sock, "edsu name-get\nname $tok_del_name\n\n");
        while (($line = fgets($sock)) !== false) {
            if ($line == "edsu name\n" || $line == "edsu oob\n") {
                // This is best efforts, so regardless of outcome, keep going   
                break;
            }
        }
    } finally {
        fclose($sock);
    }
}


function edsu_connect($username, $username_parts, $token) {
    $un_id = $username_parts[1];
    $un_host = $username_parts[2];
    $host = "$un_id.edsu.$un_host";
    $sock = stream_socket_client("ssl://$host:3216");
    if (!$sock)
        return false;
    stream_set_timeout($sock, EDSU_AUTH_CONN_TIMEOUT_S);
    return $sock;
}



function qa_get_user_email($userid) {
    return null;
}


function qa_get_userids_from_public($publicusernames) {
    $publictouserid = array();
    foreach ($publicusernames as $publicusername)
        $publictouserid[$publicusername] = $publicusername;

    return $publictouserid;
}


function qa_get_public_from_userids($userids) {
    $useridtopublic = array();

    foreach ($userids as $userid)
        $useridtopublic[$userid] = $userid;

    return $useridtopublic;
}


function qa_get_mysql_user_column_type() {
    return 'VARCHAR(192)';
}


function qa_get_login_links($relative_url_prefix, $redirect_back_to_url) {
    return array(
        'login' => null,
        'register' => null,
        'logout' => null
    );
}


function qa_get_logged_in_user_html($logged_in_user, $relative_url_prefix) {
    $publicusername = $logged_in_user['publicusername'];

    return '<a href="' . qa_path_html('user/' . $publicusername) . '" class="qa-user-link">' .
           htmlspecialchars($publicusername) . '</a>';
}


function qa_get_users_html($userids, $should_include_link, $relative_url_prefix) {
    $useridtopublic = qa_get_public_from_userids($userids);

    $usershtml = array();

    foreach ($userids as $userid) {
        $publicusername = $useridtopublic[$userid];

        $usershtml[$userid] = htmlspecialchars($publicusername);

        if ($should_include_link)
            $usershtml[$userid] = '<a href="' . qa_path_html('user/' . $publicusername) .
            '" class="qa-user-link">' . $usershtml[$userid] . '</a>';
    }

    return $usershtml;
}


function qa_avatar_html_from_userid($userid, $size, $padding) {
    return null;
}

function qa_user_report_action($userid, $action) {
}
