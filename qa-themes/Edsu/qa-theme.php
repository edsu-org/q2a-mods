<?php

class qa_html_theme extends qa_html_theme_base
{
	// use new ranking layout
	protected $ranking_block_layout = true;
	protected $theme = 'edsu';

    function suggest_next() {}
    function waiting_template() {
        $this->output('<div class="waiting-hider"><div id="qa-waiting-template" class="qa-waiting">...</div></div>');
    }

    function body_content() {
        $this->body_prefix();
        $this->notices();
        
        $this->output('<div class="top-band">');
        $this->output('<div class="top-band-content">');
        $this->output('<a href="/"><div class="logo" title="Edsu Q&amp;A"></div></a>');
        $this->search();
        $this->output('</div> <!-- END top-band-content -->');
        $this->output('</div> <!-- END top-band -->');
        $this->output('<div class="bottom-band">');
        $this->output('<div class="bottom-band-content">');
        $this->nav_main_sub();
        $this->main();
        $this->output('<!-- Side panel -->');
        $this->sidepanel();
        $this->output('<!-- End side panel -->');
        $this->output('</div> <!-- END bottom-band-content -->');
        $this->output('</div> <!-- END bottom-band -->');

        $this->body_suffix();
    }

    function sidepanel() {
        $this->output('<div class="qa-sidepanel">');
        $this->widgets('side', 'top');
        $this->sidebar();
        $this->widgets('side', 'high');
        $this->widgets('side', 'low');
        $this->output_raw(@$this->content['sidepanel']);
        edsu_show_blurb();
        $this->widgets('side', 'bottom');
        $this->output('</div>', '');
    }
}

function edsu_show_blurb() {
?>
    <div class='blurb'>
        <h2>About</h2>
        <script>
            function edsuBlurbToggleRss() {
                let el = document.querySelector('.qa-sidepanel .blurb .rss-feeds');
                el.style.display = this.shown ? 'none' : 'block';
                this.shown = !this.shown;
            }
        </script>
        <p>Edsu Q&amp;A is a place to talk about anything to do with <a href="https://edsu.org/">Edsu</a>.
        <p>It's a <a href="https://www.question2answer.org/">Question2Answer</a> site plus 
           some <a href="https://gitlab.com/edsu-org/q2a-mods">plugins</a>.</p>
        <p>Click <a href="javascript:edsuBlurbToggleRss()">here</a> to get the RSS feeds.  And if you'd like to send
           <a href="https://edclave.com/about/">me</a> feedback click <a href="/feedback">here</a>.</p>
        <div class="rss-feeds" style="display: none">
            <h2>RSS Feeds</h2>
            <p><a href="https://ask.edsu.org/feed/questions.rss">Recent questions</a></p>
            <p><a href="https://ask.edsu.org/feed/qa.rss">Recent questions and answers</a></p>
            <p><a href="https://ask.edsu.org/feed/activity.rss">Recent activity</a></p>
            <p><a href="https://ask.edsu.org/feed/hot.rss">Hot questions</a></p>
            <p><a href="https://ask.edsu.org/feed/unanswered.rss">Unanswered questions</a></p>
        </div>
    </div>
<?php
}
